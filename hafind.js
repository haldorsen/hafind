const ELEMENTS_CONTAINER_ID = 'hafindElements'; 	// ID of the container for the searchable elements
const SEARCHBOX_ID = 'hafindSearchbox';				// ID of a text input acting as the search box
const SEARCH_INTERVAL_MS = 300; 					// How often (in milliseconds) we'll look for new input in the search box

let findables = []; 	// All searchable elements
let found = [];			// Just the elements matching the search result
let searchString = ''; 	// String of characters to look for in a search

document.addEventListener("DOMContentLoaded", function () {

	// Collecting all originally searchable elements
	foundElements = document.getElementById(ELEMENTS_CONTAINER_ID);
	for (let i = 0; i < foundElements.children.length; i++) {
		findables.push(foundElements.children[i]);
	}

	// Listening for search box changes
    setInterval(keyLoop, SEARCH_INTERVAL_MS);
    function keyLoop() {
        newSearch = document.getElementById(SEARCHBOX_ID).value;
        if (newSearch != searchString) {
            searchString = newSearch;
            searchElements();
        }
    }

});

// searchElements performs a search based on searchString and findables,
// and places potential matches in the element with the ELEMENTS_CONTAINER_ID
function searchElements() {

	if (found != findables) {
		// Not all content is visible. We need to reset the ELEMENTS_CONTAINER_ID element
		document.getElementById(ELEMENTS_CONTAINER_ID).innerHTML = '';
	} else {
		// No need to do anything here
		return
	}

	// Splitting search words
    const searchWords = searchString.split(" ");

	// Search function
	found = [];
    for (let i = 0; i < findables.length; i++) {
		// Looking for matches in this element
		let foundMatch = true;
		for (let s = 0; s < searchWords.length; s++) {
			if (!findables[i].innerText.toLowerCase().match(searchWords[s].toLowerCase())) {
				foundMatch = false;
			}
		}
		// Displaying found elements
		if (foundMatch) {
			found.push(findables[i]);
			document.getElementById(ELEMENTS_CONTAINER_ID).appendChild(findables[i]);
		}
    }

	return

}
