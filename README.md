# hafind - lightweight content search

Useful if you have a manually written HTML project where you want to make some content searchable. This script won't break your webpage if the client has disabled JavaScript. There's a [demo on my website](https://ahaldorsen.no/demo/hafind) if you want to see how it works.

## How to install
Copy/paste the ```hafind.js``` file, and include it in the HTML body like this:
```html
<script src="hafind.js"></script>
```
Then add a text input with the ID ```hafindSearchbox```:
```html
<input id="hafindSearchbox" type="text" placeholder="Search">
```
And finally, add the ID ```hafindElements``` to the *parent* element of the content you want to make searchable, for example like this:

```html
<ul id="hafindElements">
```

## Usage examples
Maybe you have a list of cities you want to be searcable?
```html
<h1>Cities</h1>
<input id="hafindSearchbox" type="text" placeholder="Search">
<ul id="hafindElements">
	<li>Trondheim</li>
	<li>Kristiansand</li>
	<li>Oslo</li>
	<li>Bergen</li>
	<li>Lillehammer</li>
</ul>
<script src="hafind.js"></script>
```
...or maybe a list of participants?
```html
<h1>Participants</h1>
<input id="hafindSearchbox" type="text" placeholder="Search">
<div id="hafindElements">
	<div>
		<img src="anne.jpg">
		<div>Anne Olsen</div>
	</div>
	<div>
		<img src="emma.jpg">
		<div>Emma Kristiansen</div>
	</div>
	<div>
		<img src="paal.jpg">
		<div>Pål Berg</div>
	</div>
	<div>
		<img src="siri.jpg">
		<div>Siri Fjeld</div>
	</div>
	<div>
		<img src="tobias.jpg">
		<div>Tobias Steinkjær</div>
	</div>
</div>
<script src="hafind.js"></script>
```
